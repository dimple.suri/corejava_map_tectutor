	package employeepkg;
	import java.util.*;//I am importing all classes under util package.
	public class Calculator {
		int number1;
		int number2;
		Scanner scobj;//Scanner is a class and scobj is reference variable
		public void add()
		{
			scobj=new Scanner(System.in);
			//inititalizing scanner object.System.in represent keyboard
			System.out.println("Enter Number 1 Value");
			number1=scobj.nextInt();
			System.out.println("Enter Number 2 Value ");
			number2=scobj.nextInt();//is a predefined method in scanner class which can be used to take 
			//input as int value
			//nextLong,nextShort,nextByte,nextFloat
			int result=number1+number2;
			System.out.println("Result of Addition is "+result);
		}
		public static void main(String[] args) {
			Calculator calobj=new Calculator();
			calobj.add();
		}
	
	}
